List Algoritma Kriptografi yang digunakan:
1. File Substitution_cipher.php
   Urutan awal list alphabet diambil dari karakter kunci.

2. File Substitution_cipher_v2.php
   Urutan awal list alphabet di ambil dari karakter kunci, dimana:
   Mode 1: F(x) = (x + k) mod n(a)
   Mode 2: F(x) = (x * k) mod n(a)

3. File Polyalphabetic_substitution.php
   - Urutan awal list alphabet cipher 1 di ambil dari karakter kunci 1
   - Urutan awal list alphabet cipher 2 di ambil dari karakter kunci 2

4. File Vigenere_cipher.php
   Enkripsi: Nilai bilangan integer list alphabet ditambah 
             Nilai bilangin integer kunci lalu 
             di modulo sejumlah total karakter ascii yang digunakan.
   Dekripsi: Nilai bilangan integer list alphabet dikurang 
             Nilai bilangin integer kunci lalu 
             di modulo sejumlah total karakter ascii yang digunakan.

5. File Playfair_cipher.php
   - Membentuk matrik 5x5 dengan 1 karakter dijadikan bogus letter
   - Membagi plain teks menjadi blok 2 huruf
   - Rumus mendapatkan c1 dan c2 dari blok 2 huruf tsb adalah:
     1. Jika m1 dan m2 terdapat pada baris yang sama 
        dalam matrik kunci maka c1 diambil dari 1 huruf sebelah kanan m1, 
        c2 diambil dari 1 huruf sebelah kanan m2 pada matrik kunci.
     2. Jika m1 dan m2 terdapat pada kolom yang sama 
        dalam matrik maka c1 dan c2 masing-masing diambil 
        dari 1 huruf dibawah m1 dan m2 pada matrik kunci 
     3. Jika m1 dan m2 berbeda baris dan kolom dalam matrik kunci maka c1 
        diambil dari pertemuan baris m1 dan kolom m2, 
        dan c2 diambil dari pertemuan baris m2 dan kolom m1 pada matrik kunci 
     4. Jika m1 = m2 maka ciphertext adalah m1 kemudian karakter 
        tambahan yang telah ditentukan kemudian m2.

6. File Poly_xk.php
   Menggabungkan Algoritma Polyalphabetic Substitution, 
   dimana:
   - c1 diambil dari k1 
   - c2 diambil dari k2
   - c3 diambil dari F(x) = (x + k) mod n(a)
   - c4 diambil dari F(x) = (x * k) mod n(a)