<?php
/*
 * @author Meychel Danius (danywhy.sambuari@gmail.com)
 * @class Vigenere_cipher 
 * @param key
 */

include_once './helpers.php';

class Vigenere_cipher {
    
    private $arr = [];
    private $arr_formula = [];
    private $encrypted_text = '';
    private $decryted_text = '';
    private $is_encrypted = false;
    private $time_start;
    public $execution_time = 0;
    
    /*
     * Function __construct
     */
    function __construct() {
        
        
        $this->time_start = microtime(true); // for execution time
        
        /* 
         * step 1. create array of ascii printable character
         */
        for ($i = 32; $i <= 126; $i++) {
            $this->arr['ascii'][] = [
                'codeA' => $i,
                'charA' => chr($i)
            ];
        }
        
    }
    
    /* 
     * Function encrpyt
     * change plain text array to ascii code
     * match the code with the element of ascii array 
     * @return encrypted_text
     */
    function encrpyt($plain_text, $key, $display_ascii = false) {
        
        
        $arr = [];
        $arr_text = str_split($plain_text);
        $arr_key = str_split($key);
        $index = 0;
        
        /*
         * Combine array plain text with array key
         * when key < plain text, fill the rest value with iterate element of key
         * when key > plain text. just stop the value with maximum length of array plain text
         */
        for($i = 0; $i < count($arr_text); $i++) {
            $arr[] = [
                "plain_text" => $arr_text[$i],
                "key" => $arr_key[$index]
            ];
            $index++;
            if ($index == count($arr_key)) {
                $index = 0;
            } 
        }
        
        /*
         * Find plain text index and key index
         * add plain text index and key index and then mod by maximum length ascii array
         */
        for ($x = 0; $x < count($arr); $x++) {
            $c1 = $arr[$x]['plain_text'];
            $c2 = $arr[$x]['key'];
            
            $found_index_c1 = array_search($c1, array_column($this->arr['ascii'], 'charA'));
            $found_index_c2 = array_search($c2, array_column($this->arr['ascii'], 'charA'));
            $found_index = ($found_index_c1 + $found_index_c2) % count($this->arr['ascii']);
            $this->encrypted_text .= $this->arr['ascii'][$found_index]['charA'];
            
            $this->arr_formula[] = "$c1, $c2 => ($found_index_c1 + $found_index_c2) % ".count($this->arr['ascii'])."=".($found_index_c1 + $found_index_c2) % count($this->arr['ascii'])." => ".$this->arr['ascii'][$found_index]['charA'];
        }
        
        $this->execution_time = (microtime(true) - $this->time_start);
        $this->get_ascii($display_ascii);
        
        return $this->encrypted_text;
    }
    
    /* 
     * Function decrypt
     * change plain text array to ascii code
     * match the code with the element of ascii array 
     * @return decryted_text
     */
    function decrypt($encrypted_text, $key, $display_ascii = false) {
        
        $this->is_encrypted = true;
        $arr = [];
        $arr_text = str_split($encrypted_text);
        $arr_key = str_split($key);
        $index = 0;
        
        /*
         * Combine array plain text with array key
         * when key < plain text, fill the rest value with iterate element of key
         * when key > plain text. just stop the value with maximum length of array plain text
         */
        for($i = 0; $i < count($arr_text); $i++) {
            $arr[] = [
                "plain_text" => $arr_text[$i],
                "key" => $arr_key[$index]
            ];
            $index++;
            if ($index == count($arr_key)) {
                $index = 0;
            } 
        }
        
        /*
         * Find plain text index and key index
         * add plain text index and key index and then mod by maximum length ascii array
         */
        for ($x = 0; $x < count($arr); $x++) {
            $c1 = $arr[$x]['plain_text'];
            $c2 = $arr[$x]['key'];
            
            $found_index_c1 = array_search($c1, array_column($this->arr['ascii'], 'charA'));
            $found_index_c2 = array_search($c2, array_column($this->arr['ascii'], 'charA'));
            
            
            $found_index = ($found_index_c1 - $found_index_c2) % count($this->arr['ascii']);
            if ($found_index < 0) {
                $found_index += abs(count($this->arr['ascii']));
            }
            $this->decryted_text .= $this->arr['ascii'][$found_index]['charA'];
            
            $this->arr_formula[] = "$c1, $c2 => ($found_index_c1 - $found_index_c2) % ".count($this->arr['ascii'])."=".($found_index_c1 - $found_index_c2) % count($this->arr['ascii'])." => ".$this->arr['ascii'][$found_index]['charA'];
        }
        
        $this->execution_time = (microtime(true) - $this->time_start);
        $this->get_ascii($display_ascii);
        
        
        return $this->decryted_text;
    }
    
    function get_ascii($display_ascii) {
        
        $arr_text = ($this->is_encrypted ? str_split($this->decryted_text) : str_split($this->encrypted_text));
        
        if ($display_ascii) {
            /****************************/
            /** PoC : roof of Concept **/
            /****************************/
            $data_table = "";
            for ($i = 0; $i < count($this->arr['ascii']); $i++) {
                
                $index = $i;
                $char_ascii = $this->arr['ascii'][$i]['charA'];
                $border = ($i != 0 ? '' : 'border-top: 2px solid #000;') ;
                $check_char = in_array($char_ascii, $arr_text);
                $font_weight = ($check_char ? 'font-size:16px;font-weight:bold;color:red' : '');
                
                $data_table .= '
                    <tr>
                        <td style="text-align: center;'.$border.$font_weight.'">'.$index.'</td>
                        <td style="text-align: center;'.$border.$font_weight.'">'.$char_ascii.'</td>
                    </tr>
                ';
            }
            $html = '
                <style>
                    .cart {
                        padding:10px;
                        margin:0;
                    }
                    .cart table {
                        border-collapse:collapse;
                    }
                    .cart th {
                        padding:5px;
                        background-image:url("images/white-top-bottom-gray.gif");
                        border-color:#a4a4a4;
                        border-width:0 1px 0 0 !important;
                        border-style: none solid solid;
                        color:#333;
                        font-family:tahoma,arial,verdana,sans-serif;
                        font-size:11px;
                        font-weight:bold;
                        text-align:center;
                    }
                    .cart th:first-child {
                        border-width:0 1px 0 1px !important;
                    }
                    .cart td {
                        padding:3px 5px;
                        border-color:#99BBE8;
                        border:1px solid #ccc;
                        color:#333;
                        font-family:tahoma,arial,verdana,sans-serif;
                        font-size:11px;
                        font-weight:normal;
                    }
                    .cart th:hover {
                        border-color:#84a0c4;
                        background-image:url("images/white-top-bottom.gif");
                    }
                    .cart tr:hover {
                        background-color:#efefef;
                    }
                    .cart td strong {
                        font-weight:bold;
                    }
                    .center {
                        border-collapse: collapse;width: 500px;margin-left: auto;margin-right: auto;
                    }
                </style>
                <div class="cart" align="center">
                    <table class="center">
                        <tr>
                            <th>Index</th>
                            <th>ASCII</th>
                        </tr>
                        '.$data_table.'
                    </table>
                </div>
            ';
            echo $html.str_repeat(PHP_EOL, 20);
        }
    }
    
    function get_output($input_text, $output_text) {
        
        $label_output = ($this->is_encrypted ? "Dekripsi" : "Enkripsi");
        
        echo str_repeat("<br>", 5);
        echo '<div style="text-align:center;font-size:18px;">';
        echo "Inputan: <span style='color:red;'>$input_text</span> <br>";
        echo "Hasil $label_output: <span style='color:red;'>$output_text</span> <br>";
        echo "Total execution time in seconds: $this->execution_time <br>";
        echo "Formula: <br>";
        foreach ($this->arr_formula as $data) {
            echo "<span style='color:red;font-size:14px'>".$data.'</span><br>';
        }
        echo str_repeat("<br>", 100);
        echo '</div>';
    }
}


//$key = "ISTA"; // give whatever key you want
$key = "MEYCHEL DANIUS"; // give whatever key you want
//$key = "Meychel Danius"; // give whatever key you want
$text = 'RAHASIA'; // give whatever text you want

$subs = new Vigenere_cipher();
$encrypted_text = $subs->encrpyt($text, $key, true);
$subs->get_output($text, $encrypted_text);


//$encrypted_text = 'r "vI~$ny))f{';
//$encrypted_text = 'rxz+ v~z!+)j%';
//$plain_text = $subs->decrypt($encrypted_text, $key, true);
//$subs->get_output($encrypted_text, $plain_text);