<?php

/*
 * @author Meychel Danius (danywhy.sambuari@gmail.com)
 * @class Playfair_cipher 
 */

include_once './helpers.php';

class Playfair_cipher {
    
    private $arr = [];
    private $arr_cipher = [];
    private $arr_text = [];
    private $bogus = '';
    private $encrypted_text = '';
    private $decryted_text = '';
    private $is_encrypted = false;
    private $time_start;
    public $execution_time = 0;
    
    function __construct($plain_text, $bogus_letter = 'Q', $is_encrypted) {
        
        $this->time_start = microtime(true); // for execution time
        $this->bogus = $bogus_letter;
        if ($is_encrypted == 'E') {
            $this->is_encrypted = false;
        } else if ($is_encrypted == 'D') {
            $this->is_encrypted = true;
        }
        
        // The matrix
//        $this->arr = [
//            ['E', 'F', 'I', 'D', 'R'],
//            ['P', 'Z', 'B', 'M', 'N'],
//            ['W', 'L', 'H', 'S', 'K'],
//            ['U', 'C', 'V', 'Y', 'O'],
//            ['A', 'J', 'X', 'T', 'G'],
//        ];
        $this->arr = [
            ['W', 'S', 'H', 'F', 'M'],
            ['J', 'P', 'C', 'N', 'K'],
            ['V', 'T', 'A', 'R', 'L'],
            ['G', 'E', 'B', 'Y', 'O'],
            ['I', 'U', 'X', 'D', 'Z'],
        ];
        
        $clean_str = str_replace(' ', '', $plain_text); // remove spaces
        
//        echo $clean_str.'<br>';
        if ($this->is_encrypted) {
            // for decrypt only, for detect character X between the same character
            $clean_str = str_replace($this->bogus, '', $clean_str);
//            $positions = [];
//            $lastPos = 0;
//            
//            while (($lastPos = strpos($clean_str, 'X', $lastPos)) !== false) {
//                $positions[] = $lastPos;
//                $lastPos = $lastPos + strlen('X');
//            }
//            foreach ($positions as $value) {
//                $char = substr($clean_str, $value, 1);
//                $char_front = substr($clean_str, $value+1, 1);
//                $char_back = substr($clean_str, $value-1, 1);
//
//                if ($char_back == $char_front) {
//                    $clean_str = substr_replace($clean_str, '', $value, 1);
//                }
//            }
        }
        
        $this->arr_text = str_split($clean_str, 2);
        
        $index_m1 = [];
        $index_m2 = [];
        
        foreach($this->arr_text as $row => $data) {
            $str_count =  strlen($data);
            
            if ($str_count < 2) {
                $this->arr_text[$row] = $data.$this->bogus;
            }
            
            $m1 = strtoupper(substr($this->arr_text[$row], 0, 1));
            $m2 = strtoupper(substr($this->arr_text[$row], 1, 1));
            
            $found_m1 = $this->find_arr_value($this->arr, $m1);
            $found_m2 = $this->find_arr_value($this->arr, $m2);
            
            if ($found_m1) {
                $found_m1_split = explode(' ', $found_m1);
                $index_m1[] = [$found_m1_split[0], $found_m1_split[1], $m1, $row];
            }
            
            if ($found_m2) {
                $found_m2_split = explode(' ', $found_m2);
                $index_m2[] = [$found_m2_split[0], $found_m2_split[1], $m2, $row];
            }
            
            if ($m2 == $this->bogus) {
                $index_m2[$row] = [-1, -1, $this->bogus];
            }
            
        }
        $this->rules($index_m1, $index_m2, $this->arr_text);
    }
    
    function find_arr_value($arr, $value) {
        
       for($i = 0; $i < count($arr); $i++) {
           for ($k = 0; $k < count($arr[$i]); $k++) {
               
               if ($arr[$i][$k] == $value) {
                   return $i.' '.$k;
               }
           }
       }
    }
    
    function rules($arr1, $arr2, $arr_text) {
        for ($i = 0; $i < count($arr_text); $i++) {
            $baris_m1 = $arr1[$i][0];
            $baris_m2 = $arr2[$i][0];
            $kolom_m1 = $arr1[$i][1];
            $kolom_m2 = $arr2[$i][1];
            
            if ($arr1[$i][2] == $arr2[$i][2]) {
                
                // Jika m1 = m2 maka ciphertext adalah m1 kemudian karakter tambahan yang telah ditentukan kemudian m2  
                
                $c1 = $arr1[$i][2];
                $c2 = $arr2[$i][2];
                if ($this->is_encrypted == false) {
                    $this->arr_cipher[] = [$c1.$this->bogus, $c2, $c1.$this->bogus, $c2, 'SAMA SEMUA'];
                } else {
                    $this->arr_cipher[] = [$c1, $c2, $c1, $c2, 'SAMA SEMUA'];
                }
                
                
            } else if ($baris_m1 == $baris_m2) {
                
                // jika m1 dan m2 terdapat pada baris yang sama dalam matrik kunci maka c1 diambil dari 1 huruf sebelah kanan m1, c2 diambil dari 1 huruf sebelah kanan m2 pada matrik kunci.
                
                if (isset($this->arr[$baris_m1][$kolom_m1+1])) {
                    $c1 = $this->arr[$baris_m1][$kolom_m1+1];
                } else {
                    $c1 = $this->arr[$baris_m1][0];
                }
                if (isset($this->arr[$baris_m2][$kolom_m2+1])) {
                    $c2 = $this->arr[$baris_m2][$kolom_m2+1];
                } else {
                    $c2 = $this->arr[$baris_m2][0];
                }
                $this->arr_cipher[] = [$c1, $c2, $arr1[$i][2], $arr2[$i][2], 'BARIS SAMA'];
                
            } else if ($kolom_m1 == $kolom_m2) {
                
                // jika m1 dan m2 terdapat pada kolom yang sama dalam matrik maka c1 dan c2 masing-masing diambil dari 1 huruf dibawah m1 dan m2 pada matrik kunci.
                
                if (isset($this->arr[$baris_m1+1][$kolom_m1])) {
                    $c1 = $this->arr[$baris_m1+1][$kolom_m1];
                } else {
                    $c1 = $this->arr[0][$kolom_m1];
                }
                if (isset($this->arr[$baris_m2+1][$kolom_m2])) {
                    $c2 = $this->arr[$baris_m2+1][$kolom_m2];
                } else {
                    $c2 = $this->arr[0][$kolom_m2];
                }
                $this->arr_cipher[] = [$c1, $c2, $arr1[$i][2], $arr2[$i][2], 'KOLOM SAMA'];
                
            } else {
                
                if ($arr2[$i][2] == $this->bogus) {
                    $c1 = $arr_text[$i][0];
                    $c2 = $this->bogus;
                } else {
                    // Jika m1 dan m2 berbeda baris dan kolom
                    $c1 = $this->arr[$baris_m1][$kolom_m2];
                    $c2 = $this->arr[$baris_m2][$kolom_m1];
                }
                
                $this->arr_cipher[] = [$c1, $c2, $arr1[$i][2], $arr2[$i][2], 'BEDA BARIS, BEDA KOLOM'];
            }
        }
    }
    
    /* 
     * Function encrpyt
     * @return encrypted_text
     */
    function encrpyt() {
        
        foreach($this->arr_cipher as $data) {
            $this->encrypted_text .= $data[0].$data[1];
        }
        $this->execution_time = (microtime(true) - $this->time_start);
        
        return $this->encrypted_text;
    }
    
    function decrypt() {
        
        $this->is_encrypted = true;
        
        foreach($this->arr_cipher as $data) {
            $this->decryted_text .= $data[0].$data[1];
        }
        $this->execution_time = (microtime(true) - $this->time_start);
        
        return $this->decryted_text;
    }
    
    function get_output($input_text, $output_text) {
        
        $label_output = ($this->is_encrypted ? "Dekripsi" : "Enkripsi");
        $split = $plit_output_text = '';
        foreach($this->arr_cipher as $data => $row) {
            $split .= "<span style='color:red;'>|</span>".$row[2].''.$row[3];
            $plit_output_text .= "<span style='color:red;'>|</span>".$row[0].''.$row[1];
        }
        
        echo str_repeat("<br>", 2);
        echo '<div style="text-align:center;font-size:18px;">';
        echo "Inputan: <span style='color:red;'>$input_text</span> <br>";
        echo "Split Inputan: &nbsp;&nbsp;$split <br>";
        echo "Split $label_output: $plit_output_text <br>";
        echo "Hasil $label_output: <span style='color:red;'>$output_text</span> <br>";
        echo "Total execution time in seconds: $this->execution_time". str_repeat("<br>", 10);
        echo '</div>';
    }
    
    function show_matrik_table() {
        
        $arr_text = [];
        
        
        foreach($this->arr_cipher as $data => $row_data) {
            $arr_text[] = $row_data[2];
            $arr_text[] = $row_data[3];
        }
        
        $data_table = '';
        $font_weight = '';
        
        for ($i = 0; $i < count($this->arr); $i++) {
            
            $tr = '<tr>';
            if ($i == 0) {
                $tr .= '<td>&nbsp</td>';
                for ($x = 0; $x < count($this->arr); $x++) {
                    $tr .= '
                        <td style="text-align: center;">'.$x.'</td>
                    ';
                }
            }
            $tr .= '</tr>';
            
            $data_table .= $tr.'<tr><td style="text-align: center;">'.$i.'</td>';
            
            for ($j = 0; $j < count($this->arr[$i]); $j++) {
                
                $check_char = in_array($this->arr[$i][$j], $arr_text);
                $font_weight = ($check_char ? 'font-size:16px;font-weight:bold;color:red;' : '');
                
                $data_table .= '<td style="text-align: center;'.$font_weight.'">'.$this->arr[$i][$j].'</td>';
            }
            $data_table .= '</tr>';
        }
        
        $html = '
            <style>
                .cart {
                    padding:10px;
                    margin:0;
                }
                .cart table {
                    border-collapse:collapse;
                }
                .cart th {
                    padding:5px;
                    background-image:url("images/white-top-bottom-gray.gif");
                    border-color:#a4a4a4;
                    border-width:0 1px 0 0 !important;
                    border-style: none solid solid;
                    color:#333;
                    font-family:tahoma,arial,verdana,sans-serif;
                    font-size:11px;
                    font-weight:bold;
                    text-align:center;
                }
                .cart th:first-child {
                    border-width:0 1px 0 1px !important;
                }
                .cart td {
                    padding:3px 5px;
                    border-color:#99BBE8;
                    border:1px solid #ccc;
                    color:#333;
                    font-family:tahoma,arial,verdana,sans-serif;
                    font-size:11px;
                    font-weight:normal;
                }
                .cart th:hover {
                    border-color:#84a0c4;
                    background-image:url("images/white-top-bottom.gif");
                }
                .cart tr:hover {
                    background-color:#efefef;
                }
                .cart td strong {
                    font-weight:bold;
                }
                .center {
                    border-collapse: collapse;width: 500px;margin-left: auto;margin-right: auto;
                }
            </style>
            <div class="cart" align="center">
                <table class="center">
                    <tr>
                        <th colspan="6">MATRIK KUNCI</th>
                    </tr>
                    '.$data_table.'
                </table>
            </div>
        ';
        echo $html.str_repeat(PHP_EOL, 20);
    }
    
    /*
     * Function generate_matrix
     * @param $bogus_letter
     * this function could help you to generate the 5 x 5 matrix
     */
    function generate_matrix($bogus_letter) {
        // generate alphabet
        $arr_alpha = range('A', 'Z'); 
        $index = array_search($bogus_letter, $arr_alpha);
        
        // delete the bogus letter from alphabet array
        unset($arr_alpha[$index]); 
        // randomizes the order of the elements in) an array
        shuffle($arr_alpha); 
        
        // create the 5 x 5 matrix 
        $arr_matrix = array_chunk($arr_alpha, 5); 
        
        return $arr_matrix;
    }
}

$bogus_letter = "Q"; // give whatever character that you don't use / Bogus Letter
$key = "MEYCHEL DANIUS"; // give whatever key you want
//$text = 'UJIAN AKHIR EMPAT MINGGU LAGI'; // give whatever text you want
$text = 'RAHASIA'; // give whatever text you want

$subs = new Playfair_cipher($text, $bogus_letter, 'E');
$encrypted_text = $subs->encrpyt();
$subs->show_matrik_table();
$subs->get_output($text, $encrypted_text);
//$matrix = $subs->generate_matrix($bogus_letter);
//var_dump($matrix);

//$encrypted_text = 'IPXVCRCMDVOSCTLSDJGQGZTVBIQ';
//$subs = new Playfair_cipher($encrypted_text, $bogus_letter, 'D');
//$plain_text = $subs->decrypt();
//$subs->show_matrik_table();
//$subs->get_output($encrypted_text, $plain_text);