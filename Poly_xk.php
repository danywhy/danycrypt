<?php
/*
 * @author: Meychel Danius (danywhy.sambuari@gmail.com)
 * @class Polyalphabetic_substitution 
 * @param array key
 */

include_once './helpers.php';

class Poly_xk {
    
    private $arr = [];
    private $arr_keys = [];
    private $simple_arr = [];
    private $encrypted_text = '';
    private $decryted_text = '';
    private $is_encrypted = false;
    private $time_start;
    public $execution_time = 0;
    
    /*
     * Function __construct
     * @param $arr_key
     * @param $display_ascii default false
     */
    function __construct($arr_key) {
        
        $this->time_start = microtime(true); // for execution time
        
        /* 
         * step 0. remove duplicate character from key
         */
        foreach ($arr_key as $key) {
            $created_array = str_split($key);
            $this->arr_keys[] = array_values(array_unique($created_array));
        }
        

        /* 
         * step 1. create array of ascii printable character
         */
        $main_index = 0;
        $total_index = count($this->arr_keys) + 2;
        for ($i = 32; $i <= 126; $i++) {
            $this->arr['ascii'][$main_index] = [
                'codeA' => $i,
                'charA' => chr($i),
            ];
            for ($j = 1; $j <= $total_index; $j++) {
                $this->arr['ascii'][$main_index]['codeF'.$j] = '';
                $this->arr['ascii'][$main_index]['charF'.$j] = '';
            }
            $main_index++;
        }

        /* 
         * step 2. create substitution array of ascii printable character 
         * where the encryption key is the starting index for each key
         */
        for ($i = 0; $i < count($this->arr_keys); $i++) {
            
            for ($j = 0; $j < count($this->arr_keys[$i]); $j++) {
                $this->arr['ascii'][$j]['codeF'.($i+1)] = ord($this->arr_keys[$i][$j]);
                $this->arr['ascii'][$j]['charF'.($i+1)] = $this->arr_keys[$i][$j];
                
                $this->simple_arr[$i][$this->arr_keys[$i][$j]] = $this->arr_keys[$i][$j];
            }
        }
        
        
        // just keep the counter of index, 
        // so that we know where we are going to continue the substitution array
        $arr_index = [];
        for ($i = 0; $i < count($this->arr_keys); $i++) {
            $arr_index[] = count($this->arr_keys[$i]);
        }
        
        /* 
         * step 3. continue create substitution array 
         * where the start index is the last index from the last array 
         * and check the value against the whole character in original ascii array 
         * if character is duplicate, skip to next character
         */
        $k = 9;
        /*
         * mode 1 = f(x) = (x + k) mod n(alpha)
         * mode 2 = f(x) = (x * k) mod n(alpha)
         */
        $operations = array('+' => 'add', '*' => 'multiply');
//        $operator = ($mode == 1 ? '+' : '*');
        $total_ascii_index = count($this->arr['ascii']);
        
        for ($i = 0; $i < $total_ascii_index; $i++) {
            
            for ($x = 0; $x < count($this->arr_keys); $x++) {
                
                if (!isset($this->simple_arr[$x][$this->arr['ascii'][$i]['charA']])) {
                    
                    $this->simple_arr[$x][$this->arr['ascii'][$i]['charA']] = $this->arr['ascii'][$i]['charA'];
                    $this->arr['ascii'][$arr_index[$x]]['charF'.($x+1)] = $this->arr['ascii'][$i]['charA'];
                    $this->arr['ascii'][$arr_index[$x]]['codeF'.($x+1)] = $this->arr['ascii'][$i]['codeA'];
                    
                    $arr_index[$x] = $arr_index[$x] + 1;
                }
            }
            
            // key 3
            $index_subs = ($operations['+']($i, $k)) % $total_ascii_index;
            $this->arr['ascii'][$i]['charF3'] = $this->arr['ascii'][$index_subs]['charA'];
            $this->arr['ascii'][$i]['codeF3'] = $this->arr['ascii'][$index_subs]['codeA'];
            // key 4
            $index_subs = ($operations['*']($i, $k)) % $total_ascii_index;
            $this->arr['ascii'][$i]['charF4'] = $this->arr['ascii'][$index_subs]['charA'];
            $this->arr['ascii'][$i]['codeF4'] = $this->arr['ascii'][$index_subs]['codeA'];
        }
    }
    
    /* 
     * Function encrpyt
     * change plain text array to ascii code
     * match the code with the element of substitution array 
     * @return encrypted_text
     */
    function encrpyt($plain_text, $display_ascii = false) {
        
        
        $arr_text = str_split($plain_text);
        $index_flag = 0;
        $max_index = count($this->arr_keys) + 2;
        
        foreach($arr_text as $data) {
            $found_index = array_search($data, array_column($this->arr['ascii'], 'charA'));
            
            $this->encrypted_text .= $this->arr['ascii'][$found_index]['charF'.($index_flag+1)];
            $index_flag++;
            if ($index_flag == $max_index) {
                $index_flag = 0;
            }
        }
        $this->execution_time = (microtime(true) - $this->time_start);
        $this->get_ascii($display_ascii);
        
        return $this->encrypted_text;
    }
    
    /* 
     * Function decrypt
     * change encrypted text array to ascii code
     * match the code with the element of ascii array 
     * @return decryted_text
     */
    function decrypt($encrypted_text, $display_ascii = false) {
        
        $this->is_encrypted = true;
        $arr_text = str_split($encrypted_text);
        $index_flag = 0;
        $max_index = count($this->arr_keys);
        
        foreach($arr_text as $data) {
            $found_index = array_search($data, array_column($this->arr['ascii'], 'charF'.($index_flag+1)));
            $this->decryted_text .= $this->arr['ascii'][$found_index]['charA'];
            
            $index_flag++;
            
            if ($index_flag == $max_index) {
                $index_flag = 0;
            }
        }
        $this->execution_time = (microtime(true) - $this->time_start);
        $this->get_ascii($display_ascii);
        
        return $this->decryted_text;
    }
    
    function get_ascii($display_ascii) {
        
        $arr_text = ($this->is_encrypted ? str_split($this->decryted_text) : str_split($this->encrypted_text));
        
        if ($display_ascii) {
            /****************************/
            /** PoC : roof of Concept **/
            /****************************/
            $data_table = "";
            $total_index = count($this->arr_keys) + 2;
            
            for ($i = 0; $i < count($this->arr['ascii']); $i++) {
                
                $index = $i;
                $char_ascii = $this->arr['ascii'][$i]['charA'];
                $char_subs = [];
                $font_weight = '';
                $border = ($i != 0 ? '' : 'border-top: 2px solid #000;') ;
                $cols = '';
                $rows = '';
                
                for ($x = 0; $x < $total_index; $x++) {
                    
                    $char_subs = $this->arr['ascii'][$i]['charF'.($x+1)];
                    
                    $check_char = ($this->is_encrypted ? in_array($char_ascii, $arr_text) : in_array($char_subs, $arr_text));
                    $font_weight = ($check_char ? 'font-size:16px;font-weight:bold;color:red' : '');
                    $cols .= '<th>F'.($x+1).'</th>';
                    
                    $rows .= '<td style="text-align: center;'.$border.$font_weight.'">'.$char_subs.'</td>';
                }
                
                $data_table .= '
                    <tr>
                        <td style="text-align: center;'.$border.$font_weight.'">'.$index.'</td>
                        <td style="text-align: center;'.$border.$font_weight.'">'.$char_ascii.'</td>
                        '.$rows.'
                    </tr>
                ';
            }
            $html = '
                <style>
                    .cart {
                        padding:10px;
                        margin:0;
                    }
                    .cart table {
                        border-collapse:collapse;
                    }
                    .cart th {
                        padding:5px;
                        background-image:url("images/white-top-bottom-gray.gif");
                        border-color:#a4a4a4;
                        border-width:0 1px 0 0 !important;
                        border-style: none solid solid;
                        color:#333;
                        font-family:tahoma,arial,verdana,sans-serif;
                        font-size:11px;
                        font-weight:bold;
                        text-align:center;
                    }
                    .cart th:first-child {
                        border-width:0 1px 0 1px !important;
                    }
                    .cart td {
                        padding:3px 5px;
                        border-color:#99BBE8;
                        border:1px solid #ccc;
                        color:#333;
                        font-family:tahoma,arial,verdana,sans-serif;
                        font-size:11px;
                        font-weight:normal;
                    }
                    .cart th:hover {
                        border-color:#84a0c4;
                        background-image:url("images/white-top-bottom.gif");
                    }
                    .cart tr:hover {
                        background-color:#efefef;
                    }
                    .cart td strong {
                        font-weight:bold;
                    }
                    .center {
                        border-collapse: collapse;width: 500px;margin-left: auto;margin-right: auto;
                    }
                </style>
                <div class="cart" align="center">
                    <table class="center">
                        <tr>
                            <th>Index</th>
                            <th>ASCII</th>
                            '.$cols.'
                        </tr>
                        '.$data_table.'
                    </table>
                </div>
            ';
            echo $html.str_repeat(PHP_EOL, 20);
        }
    }
    
    function get_output($input_text, $output_text) {
        
        $label_output = ($this->is_encrypted ? "Dekripsi" : "Enkripsi");
        
        echo str_repeat("<br>", 5);
        echo '<div style="text-align:center;font-size:18px;">';
        echo "Inputan: <span style='color:red;'>$input_text</span> <br>";
        echo "Hasil $label_output: <span style='color:red;'>$output_text</span> <br>";
        echo "Total execution time in seconds: $this->execution_time". str_repeat("<br>", 100);
        echo '</div>';
    }
}


$arr_key = ["EMY SETYANINGSIH", "MALECITA NUR ATALA"]; // key-1, key-2,.., key-n
//$key = "Meychel Danius"; // give whatever key you want
$text = 'CRYPTOGRAPHY'; // give whatever text you want

$subs = new Poly_xk($arr_key);
$encrypted_text = $subs->encrpyt($text, true);
$subs->get_output($text, $encrypted_text);


//$encrypted_text = '9OXJQH=O7J>Y';
//$plain_text = $subs->decrypt($encrypted_text, true);
//$subs->get_output($encrypted_text, $plain_text);