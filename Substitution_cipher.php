<?php
/*
 * @author: Meychel Danius (danywhy.sambuari@gmail.com)
 * @class Substitution_cipher 
 * @param key
 */

include_once './helpers.php';

class Polyalphabetic_substitution {
    
    private $arr = [];
    private $simple_arr = [];
    private $encrypted_text = '';
    private $decryted_text = '';
    private $is_encrypted = false;
    private $time_start;
    public $execution_time = 0;
    
    /*
     * Function __construct
     * @param $key
     * @param $display_ascii default false
     */
    function __construct($key) {
        
        $this->time_start = microtime(true); // for execution time
        
        /* 
         * step 0. remove duplicate character from key
         */
        $created_array = str_split($key);
        $arr_key = array_values(array_unique($created_array));

        /* 
         * step 1. create array of ascii printable character
         */
        for ($i = 32; $i <= 126; $i++) {
            $this->arr['ascii'][] = [
                'codeA' => $i,
                'charA' => chr($i),
                'codeC' => '',
                'charC' => ''
            ];
        }

        /* 
         * step 2. create substitution array of ascii printable character 
         * where the encryption key is the starting index
         */
        for ($i = 0; $i < count($arr_key); $i++) {
            $this->arr['ascii'][$i]['codeC'] = ord($arr_key[$i]);
            $this->arr['ascii'][$i]['charC'] = $arr_key[$i];
            $this->simple_arr[$arr_key[$i]] = $arr_key[$i];
        }

        // just keep the counter of index, 
        // so that we know where we are going to continue the substitution array
        $index = count($arr_key);

        /* 
         * step 3. continue create substitution array 
         * where the start index is the last index from the last array 
         * and check the value against the whole character in original ascii array 
         * if character is duplicate, skip to next character
         */
        for ($i = 0; $i < count($this->arr['ascii']); $i++) {
            if (!isset($this->simple_arr[$this->arr['ascii'][$i]['charA']])) {
                $this->simple_arr[$this->arr['ascii'][$i]['charA']] = $this->arr['ascii'][$i]['charA'];
                $this->arr['ascii'][$index]['charC'] = $this->arr['ascii'][$i]['charA'];
                $this->arr['ascii'][$index]['codeC'] = $this->arr['ascii'][$i]['codeA'];
                $index++;
            }
        }
        
    }
    
    /* 
     * Function encrpyt
     * change plain text array to ascii code
     * match the code with the element of substitution array 
     * @return encrypted_text
     */
    function encrpyt($plain_text, $display_ascii = false) {
        
        
        $arr_text = str_split($plain_text);
        
        foreach($arr_text as $data) {
            $found_index = array_search($data, array_column($this->arr['ascii'], 'charA'));
            $this->encrypted_text .= $this->arr['ascii'][$found_index]['charC'];
        }
        $this->execution_time = (microtime(true) - $this->time_start);
        $this->get_ascii($display_ascii);
        
        return $this->encrypted_text;
    }
    
    function decrypt($encrypted_text, $display_ascii = false) {
        
        $this->is_encrypted = true;
        $arr_text = str_split($encrypted_text);
        
        foreach($arr_text as $data) {
            $found_index = array_search($data, array_column($this->arr['ascii'], 'charC'));
            $this->decryted_text .= $this->arr['ascii'][$found_index]['charA'];
        }
        $this->execution_time = (microtime(true) - $this->time_start);
        $this->get_ascii($display_ascii);
        
        return $this->decryted_text;
    }
    
    function get_ascii($display_ascii) {
        
        $arr_text = ($this->is_encrypted ? str_split($this->decryted_text) : str_split($this->encrypted_text));
        
        if ($display_ascii) {
            /****************************/
            /** PoC : roof of Concept **/
            /****************************/
            $data_table = "";
            for ($i = 0; $i < count($this->arr['ascii']); $i++) {
                
                $index = $i;
                $char_ascii = $this->arr['ascii'][$i]['charA'];
                $char_subs = $this->arr['ascii'][$i]['charC'];
                $border = ($i != 0 ? '' : 'border-top: 2px solid #000;') ;
                
                $check_char = ($this->is_encrypted ? in_array($char_ascii, $arr_text) : in_array($char_subs, $arr_text));
                $font_weight = ($check_char ? 'font-size:16px;font-weight:bold;color:red' : '');
                
                $data_table .= '
                    <tr>
                        <td style="text-align: center;'.$border.$font_weight.'">'.$index.'</td>
                        <td style="text-align: center;'.$border.$font_weight.'">'.$char_ascii.'</td>
                        <td style="text-align: center;'.$border.$font_weight.'">'.$char_subs.'</td>
                    </tr>
                ';
            }
            $html = '
                <style>
                    .cart {
                        padding:10px;
                        margin:0;
                    }
                    .cart table {
                        border-collapse:collapse;
                    }
                    .cart th {
                        padding:5px;
                        background-image:url("images/white-top-bottom-gray.gif");
                        border-color:#a4a4a4;
                        border-width:0 1px 0 0 !important;
                        border-style: none solid solid;
                        color:#333;
                        font-family:tahoma,arial,verdana,sans-serif;
                        font-size:11px;
                        font-weight:bold;
                        text-align:center;
                    }
                    .cart th:first-child {
                        border-width:0 1px 0 1px !important;
                    }
                    .cart td {
                        padding:3px 5px;
                        border-color:#99BBE8;
                        border:1px solid #ccc;
                        color:#333;
                        font-family:tahoma,arial,verdana,sans-serif;
                        font-size:11px;
                        font-weight:normal;
                    }
                    .cart th:hover {
                        border-color:#84a0c4;
                        background-image:url("images/white-top-bottom.gif");
                    }
                    .cart tr:hover {
                        background-color:#efefef;
                    }
                    .cart td strong {
                        font-weight:bold;
                    }
                    .center {
                        border-collapse: collapse;width: 500px;margin-left: auto;margin-right: auto;
                    }
                </style>
                <div class="cart" align="center">
                    <table class="center">
                        <tr>
                            <th>Index</th>
                            <th>ASCII</th>
                            <th>Substitute</th>
                        </tr>
                        '.$data_table.'
                    </table>
                </div>
            ';
            echo $html.str_repeat(PHP_EOL, 20);
        }
    }
    
    function get_output($input_text, $output_text) {
        
        $label_output = ($this->is_encrypted ? "Dekripsi" : "Enkripsi");
        
        echo str_repeat("<br>", 5);
        echo '<div style="text-align:center;font-size:18px;">';
        echo "Inputan: <span style='color:red;'>$input_text</span> <br>";
        echo "Hasil $label_output: <span style='color:red;'>$output_text</span> <br>";
        echo "Total execution time in seconds: $this->execution_time". str_repeat("<br>", 100);
        echo '</div>';
    }
}


$key = "EMY SETYANINGSIH"; // give whatever key you want
//$key = "Meychel Danius"; // give whatever key you want
$text = 'CRYPTOGRAPHY'; // give whatever text you want

$subs = new Polyalphabetic_substitution($key);
//$encrypted_text = $subs->encrpyt($text, true);
//$subs->get_output($text, $encrypted_text);


//$encrypted_text = '9OXKQJ=O7K>X';
//$plain_text = $subs->decrypt($encrypted_text, true);
//$subs->get_output($encrypted_text, $plain_text);